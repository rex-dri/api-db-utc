API DB UTC
=========

Ce répo contient une application Java permettant d'exposer des WebServices sur la base de données de l'UTC dans le cadre du projet `REX-DRI`.

## Technos

- Spring Boot,
- Hibernate,
- Swagger,
- etc.

## Attention

Avant d'aller plus loin, notons que [swagger ne documente pas les bons Request Params](https://github.com/springfox/springfox/issues/2623). Pour controler la taille des pages retourner, utiliser le paramètre `size` (et non ~~`pageSize`~~) et pour le numéro de la page, utiliser le paramètre `page` (et non ~~`pageNumber`~~)

## Utilisation

Le détail des webservices mis à disposition est visible depuis l'interface de Swagger, sur la page http://localhost:8080/swagger-ui.html#/ (lorsque l'application est lancée en mode développement).

----

L'application possède deux modes de fonctionnement:

- En vue d'une connexion avec la BDD oracle de l'UTC,
- En vue d'une connexion avec une base de données en mémoire H2 préchargée avec des données (pour un usage local/en développement). 

La distinction entre les deux se fait uniquement selon un profil Spring, qui se choisit avec une simple variable d'environnement.

Une image docker contenant l'application est disponible: registry.gitlab.utc.fr/rex-dri/api-db-utc:v1.1.0


### En production (avec Oracle)

Copier le contenu du fichier `oracle.env.example` dans un fichier `oracle.env` et modifier les variables appropriées (ne touchez pas au profil). Puis lancer le conteneur Docker:

```bash
make prod
```

***Attention le fichier `docker-compose.prod.yml` utilisait dans ce cas expose le port `8080` du conteneur sur le port `8080` de la machine hôte.***


### En développement (avec H2)

(Rien de spécial n’est à faire)

```bash
make dev
```

*Le port `8080` du conteneur est également répliqué.*

En mode développement, des données exemples sont chargés dans la BDD H2. Vous pouvez changer ces données en mappant un volume dans le fichier `docker-compose.dev.yml` (voir exemple dans ce fichier).
