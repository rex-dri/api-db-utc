dev:
	docker-compose -f docker-compose.dev.yml up --build

prod:
	docker-compose -f docker-compose.prod.yml up -d

down_prod:
	docker-compose -f docker-compose.prod.yml down