FROM maven:3.6.0-jdk-11-slim as build

WORKDIR /usr/src/app

# Un peu d'optimization du dockerfile pour éviter de
# télécharger les dépendances à cahque fois.
COPY pom.xml pom.xml
COPY src/lib src/lib
RUN mvn clean
RUN mvn dependency:go-offline --fail-never

COPY src src
RUN mvn package


FROM openjdk:11-jre-slim

WORKDIR /usr/src/app
COPY --from=build /usr/src/app/target/*.jar app.jar
COPY db db
CMD java -jar app.jar
