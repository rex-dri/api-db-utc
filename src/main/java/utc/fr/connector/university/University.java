package utc.fr.connector.university;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "VW_REX_ETAB_PARTENAIRES")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@Getter
public class University {
    @Id
    @ApiModelProperty(notes = "Identifiant unique de l'université partenaire")
    private Long idEtab;

    @ApiModelProperty(notes = "Nom de l'établissement")
    private String nomEtab;

    @ApiModelProperty(notes = "Adresse de l'établissement")
    private String adr1;

    @ApiModelProperty(notes = "Complément d'adresse")
    private String adr2;

    @ApiModelProperty(notes = "Code postal")
    private String codePostal;

    @ApiModelProperty(notes = "Ville")
    private String ville;

    @ApiModelProperty(notes = "Pays")
    private String pays;

    @ApiModelProperty(notes = "Code ISO du pays")
    private String codeIso;
}
