package utc.fr.connector.destination;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface OpenedDestinationRepository extends JpaRepository<OpenedDestination, Long> {
}
