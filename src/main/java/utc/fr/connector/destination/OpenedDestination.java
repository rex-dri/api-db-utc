package utc.fr.connector.destination;


import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import utc.fr.connector.core.FrenchStringBooleanConverter;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VW_REX_DESTINATIONS_OUVERTES")
@NoArgsConstructor
@ToString
@Getter
public class OpenedDestination {

    @Id
    @ApiModelProperty(notes = "Identidiant de l'établissement concerné")
    private Long idEtab;

    @ApiModelProperty(notes = "Semestre de départ")
    private String semestre;

    @ApiModelProperty(notes = "Commentaire de la DRI")
    private String commentaire;

    @ApiModelProperty(notes = "Nombre de places disponibles")
    private Long nombrePlaces;

    @Convert(converter = FrenchStringBooleanConverter.class)
    @ApiModelProperty(notes = "Destination pouvant donné lieu à l'obtention d'un double-diplôme")
    private Boolean doubleDiplome;

    @Convert(converter = FrenchStringBooleanConverter.class)
    @ApiModelProperty(notes = "Destination pouvant donné lieu à l'obtention d'un master")
    private Boolean master;

    @ApiModelProperty(notes = "Branches concernées par la destination")
    private String branches;
}
