package utc.fr.connector;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import utc.fr.connector.course.Course;
import utc.fr.connector.course.CourseRepository;
import utc.fr.connector.destination.OpenedDestination;
import utc.fr.connector.destination.OpenedDestinationRepository;
import utc.fr.connector.exchange.Exchange;
import utc.fr.connector.exchange.ExchangeRepository;
import utc.fr.connector.university.University;
import utc.fr.connector.university.UniversityRepository;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/")
public class Api {
    private final UniversityRepository universityRepository;
    private final OpenedDestinationRepository openedDestinationRepository;
    private final ExchangeRepository exchangeRepository;
    private final CourseRepository courseRepository;


    @Autowired
    public Api(CourseRepository courseRepository, UniversityRepository universityRepository, ExchangeRepository exchangeRepository, OpenedDestinationRepository openedDestinationRepository) {
        this.courseRepository = courseRepository;
        this.universityRepository = universityRepository;
        this.exchangeRepository = exchangeRepository;
        this.openedDestinationRepository = openedDestinationRepository;
    }


    @GetMapping(value = "universities")
    @ApiOperation(value = "Lister les universités partenaires (paginé)")
    public Page<University> getUniversities(@PageableDefault Pageable pageRequest) {
        return universityRepository.findAll(pageRequest);
    }

    @GetMapping(value = "openedDestinations")
    @ApiOperation(value = "Lister les destinations ouvertes (paginé)")
    public Page<OpenedDestination> getOpenedDestinations(@PageableDefault Pageable pageRequest) {
        return openedDestinationRepository.findAll(pageRequest);
    }

    @GetMapping(value = "courses")
    @ApiOperation(value = "Lister les cours suivis à l'étanger (paginé)")
    public Page<Course> getCourses(@PageableDefault Pageable pageRequest) {
        return courseRepository.findAll(pageRequest);
    }

    @GetMapping(value = "courses/{login}")
    @ApiOperation(value = "Lister les cours suivis à l'étranger par un étudiant")
    public List<Course> getCoursesForStudent(@ApiParam(value = "Login UTC de l'étudiant", required = true) @PathVariable String login) {
        return courseRepository.findAllByLogin(login);
    }

    @GetMapping(value = "exchanges")
    @ApiOperation(value = "Lister les échanges réalisés (paginé)")
    public Page<Exchange> getExchanges(@PageableDefault Pageable pageRequest) {
        return exchangeRepository.findAll(pageRequest);
    }

    @GetMapping(value = "exchanges/{login}")
    @ApiOperation(value = "Lister les échanges réalisés par un étudiant")
    public List<Exchange> getExchangesForLogin(@ApiParam(value = "Login UTC de l'étudiant", required = true) @PathVariable String login) {
        List<Long> exchangesId = courseRepository.findAllByLogin(login)
                .stream()
                .map(Course::getIdDepart)
                .collect(Collectors.toList());
        return exchangeRepository.findAllById(exchangesId);
    }

}