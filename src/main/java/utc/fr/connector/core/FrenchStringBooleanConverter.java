package utc.fr.connector.core;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class FrenchStringBooleanConverter implements AttributeConverter<Boolean, String> {
    @Override
    public String convertToDatabaseColumn(Boolean value) {
        if (value == null) {
            return null;
        } else {
            return value ? "O" : "N";
        }
    }

    @Override
    public Boolean convertToEntityAttribute(String value) {
        if (value == null) {
            return null;
        } else {
            return "O".equals(value);
        }
    }
}