package utc.fr.connector.exchange;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import utc.fr.connector.core.FrenchStringBooleanConverter;

import javax.persistence.*;

@Entity
@Table(name = "VW_REX_echanges_realises")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@Getter
public class Exchange {
    @Id
    @ApiModelProperty(notes = "Identifiant unique de l'échange")
    private Long idDepart;

    @ApiModelProperty(notes = "Identifiant de l'établissement dans lequel l'échange a été effectué")
    private Long idEtab;

    @ApiModelProperty(notes = "Semestre de départ")
    private String semestreDepart;

    @ApiModelProperty(notes = "Durée de l'échange en semestre(s)")
    private Long duree;

    @Convert(converter = FrenchStringBooleanConverter.class)
    @ApiModelProperty(notes = "Un double-diplôme a-t-il était obtenu lors de l'échange ?")
    private Boolean doubleDiplome;

    @Convert(converter = FrenchStringBooleanConverter.class)
    @ApiModelProperty(notes = "Un master a-t-il était obtenu lors de l'échange ?")
    private Boolean master;

    @ApiModelProperty(notes = "Master, HuTech, TC ou Branche")
    private String diplome;

    @ApiModelProperty(notes = "Branche et semestre de l'étudiant lors du départ")
    private String specialite;

    @Column(name = "\"OPTION\"")
    @ApiModelProperty(notes = "Filière de l'étudiant lors du départ ou null")
    private String option;

    @Convert(converter = FrenchStringBooleanConverter.class)
    @ApiModelProperty(notes = "L'étudiant concerné a-t-il donné son autorisation pour transférer également les cours qu'il a suivi ?")
    private Boolean autorisationTransfertUv;

    @Convert(converter = FrenchStringBooleanConverter.class)
    @ApiModelProperty(notes = "L'étudiant concerné a-t-il donné l'autorisation de partagé son login ?")
    private Boolean autorisationTransfertLogin;
}