package utc.fr.connector.course;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "VW_REX_ENSEIGNEMENTS")
@NoArgsConstructor
@ToString
@Getter
public class Course {
    @Id
    @Column(name = "id_enseig")
    @ApiModelProperty(notes = "Identifiant unique du cours suivi lors d'un départ")
    private Long idCours;

    @ApiModelProperty(notes = "Identifiant unique du départ associé")
    private Long idDepart;

    @ApiModelProperty(notes = "Immatriculation locale du cours")
    private String code;

    @ApiModelProperty(notes = "Intitulé du cours")
    private String titre;

    @ApiModelProperty(notes = "Nombre de crédits ECTS")
    private BigDecimal ects;

    @ApiModelProperty(notes = "Lien vers la description du cours")
    private String lien;

    @ApiModelProperty(notes = "Catégorie du cours: CS, TM, etc.")
    private String categorie;

    @ApiModelProperty(notes = "Dans quel profil le cours a été associé: PCB, PSF, etc.")
    private String profil;

    @ApiModelProperty(notes = "Catégorie TSH du cours")
    private String categorieTsh;

    @Column(name = "\"LOGIN\"")
    @ApiModelProperty(notes = "Login UTC de l'étudiant qui a effectué le départ")
    private String login;
}
