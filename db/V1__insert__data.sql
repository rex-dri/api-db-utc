INSERT INTO REX_ETAB_PARTENAIRES (ID_ETAB, NOM_ETAB, ADR1, ADR2, CODE_POSTAL, VILLE, PAYS, CODE_ISO)
VALUES (1, 'UNIVERSITE 1', 'CENTRE-VILLE', 'ILE DE FRANCE', '75000', 'PARIS',
        'FRANCE', 'FR');
INSERT INTO REX_ETAB_PARTENAIRES (ID_ETAB, NOM_ETAB, ADR1, ADR2, CODE_POSTAL, VILLE, PAYS, CODE_ISO)
VALUES (2, 'UNIVERSITY 2', 'HONG KONG', 'HONG KONG', '25648', 'HONG KONG',
        'HONG KONG', 'HK');
INSERT INTO REX_ETAB_PARTENAIRES (ID_ETAB, NOM_ETAB, ADR1, ADR2, CODE_POSTAL, VILLE, PAYS, CODE_ISO)
VALUES (3, 'UNIVERSITY 3', 'HDA,', null, '6542', 'FALUN', 'SUEDE', 'SE');
INSERT INTO REX_ETAB_PARTENAIRES (ID_ETAB, NOM_ETAB, ADR1, ADR2, CODE_POSTAL, VILLE, PAYS, CODE_ISO)
VALUES (4, 'UNIVERSITY 4', 'SUPER ROAD', 'SUITE 1W', '566532', 'PHILADELPHIA',
        'ETATS UNIS D''AMÉRIQUE', 'US');
INSERT INTO REX_ETAB_PARTENAIRES (ID_ETAB, NOM_ETAB, ADR1, ADR2, CODE_POSTAL, VILLE, PAYS, CODE_ISO)
VALUES (5, 'UNIVERSITY 5', '25', null, '4099', 'PORTO', 'PORTUGAL', 'PT');
INSERT INTO REX_ETAB_PARTENAIRES (ID_ETAB, NOM_ETAB, ADR1, ADR2, CODE_POSTAL, VILLE, PAYS, CODE_ISO)
VALUES (6, 'UNIVERSITY 6', 'STRABE DES 17', null, '623', 'BERLIN', 'ALLEMAGNE', 'DE');
INSERT INTO REX_ETAB_PARTENAIRES (ID_ETAB, NOM_ETAB, ADR1, ADR2, CODE_POSTAL, VILLE, PAYS, CODE_ISO)
VALUES (7, 'UNIVERSITY 7', 'KANALSTRASSE 33', null, '73728', 'ESSLINGEN', 'ALLEMAGNE', 'DE');
INSERT INTO REX_ETAB_PARTENAIRES (ID_ETAB, NOM_ETAB, ADR1, ADR2, CODE_POSTAL, VILLE, PAYS, CODE_ISO)
VALUES (8, 'UNIVERSITY 8', 'KOREAKOULUNKATU 10', null, '33101', 'TAMPERE', 'FINLANDE',
        'FI');
INSERT INTO REX_ETAB_PARTENAIRES (ID_ETAB, NOM_ETAB, ADR1, ADR2, CODE_POSTAL, VILLE, PAYS, CODE_ISO)
VALUES (9, 'UNIVERSITY 9', 'PO ABAL 13,', null, null, 'SAN SEBASTIAN', 'ESPAGNE', 'ES');
INSERT INTO REX_ETAB_PARTENAIRES (ID_ETAB, NOM_ETAB, ADR1, ADR2, CODE_POSTAL, VILLE, PAYS, CODE_ISO)
VALUES (10, 'UNIVERSITY 10', 'COLLEGE OF ENGINEERING ',
        'GREEN STREET', '61801', 'IL', 'ETATS UNIS D''AMÉRIQUE', 'US');



INSERT INTO REX_DESTINATIONS_OUVERTES (ID_ETAB, SEMESTRE, COMMENTAIRE, NOMBRE_PLACES, MASTER, DOUBLE_DIPLOME, BRANCHES)
VALUES (1, 'A2019', 'Anglais: B2; FranÃ§ais: B2;
IM: Engineering and trade', 1, 'N', 'N', null);
INSERT INTO REX_DESTINATIONS_OUVERTES (ID_ETAB, SEMESTRE, COMMENTAIRE, NOMBRE_PLACES, MASTER, DOUBLE_DIPLOME, BRANCHES)
VALUES (2, 'A2019', 'A NOTER: GB filière BB, IAA;', 5, 'O', 'O', 'GB,GI,GP,IM');
INSERT INTO REX_DESTINATIONS_OUVERTES (ID_ETAB, SEMESTRE, COMMENTAIRE, NOMBRE_PLACES, MASTER, DOUBLE_DIPLOME, BRANCHES)
VALUES (3, 'A2019', 'LANGUE 1: Allemand, A2;
IM: Mechanics and metal trades (4), Electricity & energy (4);
GP: Chemical Eng & processes (2);
GSU: Building & civil engineering (2);', 8, 'N', 'N', 'GP,GSU,IM,TC');
INSERT INTO REX_DESTINATIONS_OUVERTES (ID_ETAB, SEMESTRE, COMMENTAIRE, NOMBRE_PLACES, MASTER, DOUBLE_DIPLOME, BRANCHES)
VALUES (4, 'A2019', 'LANGUE 1: Portugais, A2;
GB: (1) DD possible;
GI: (1);
IM: (3) DD possible;
GP: (1) DD possible;
GSU: (1);
A NOTER: GB filière CIB, IAA;', 7, 'N', 'O', 'GB,GI,GP,GSU,IM');


INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (1, 1, 'A2008', 1, 'N', 'N', 'Branche', 'GSU5', 'SR', 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (1, 2, 'P2009', 1, 'N', 'N', 'Branche', 'GB4', 'IAA', 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (2, 3, 'P2009', 3, 'O', 'N', 'Branche', 'GM4', 'MIT', 'O', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (2, 4, 'A2008', 1, 'N', 'N', 'Branche', 'GM5', 'MIT', 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (3, 5, 'P2009', 1, 'N', 'N', 'TC', 'TC4', null, 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (3, 6, 'A2009', 2, 'O', 'N', 'Branche', 'GSM5', 'CMI', 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (3, 7, 'A2009', 1, 'N', 'N', 'Branche', 'GSU5', 'AIE', 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (4, 8, 'A2009', 1, 'N', 'N', 'Branche', 'GSU5', 'SR', 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (4, 9, 'A2009', 1, 'N', 'N', 'Branche', 'GM2', null, 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (1, 10, 'A2009', 3, 'O', 'N', 'Branche', 'GSM5', 'CMI', 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (5, 11, 'P2010', 1, 'N', 'N', 'Branche', 'GP5', 'TE', 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (3, 12, 'A2008', 1, 'N', 'N', 'Branche', 'GM5', 'FQI', 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (2, 13, 'P2009', 1, 'N', 'N', 'TC', 'TC4', null, 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (9, 14, 'P2009', 1, 'N', 'N', 'Branche', 'GI4', 'ICSI', 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (8, 15, 'A2008', 1, 'N', 'N', 'Branche', 'GM3', null, 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (8, 16, 'P2009', 1, 'N', 'N', 'Branche', 'GI4', 'ICSI', 'N', 'N');
INSERT INTO REX_ECHANGES_REALISES (ID_ETAB, ID_DEPART, SEMESTRE_DEPART, DUREE, DOUBLE_DIPLOME, MASTER, DIPLOME,
                                   SPECIALITE, "OPTION", AUTORISATION_TRANSFERT_UV, AUTORISATION_TRANSFERT_LOGIN)
VALUES (6, 17, 'P2009', 1, 'O', 'O', 'Branche', 'GSM4', 'MPI', 'N', 'N');


INSERT INTO REX_ENSEIGNEMENTS (ID_DEPART, ID_ENSEIG, CODE, TITRE, LIEN, ECTS, CATEGORIE, PROFIL, CATEGORIE_TSH, LOGIN)
VALUES (17, 1, 'HUM-422', 'Understanding modern Switzerland I',
        'http://edu.epfl.ch/coursebook/en/understanding-modern-switzerland-i-HUM-422-A', 3.00, 'TSH', null, 'COM-DP',
        'floflo');
INSERT INTO REX_ENSEIGNEMENTS (ID_DEPART, ID_ENSEIG, CODE, TITRE, LIEN, ECTS, CATEGORIE, PROFIL, CATEGORIE_TSH, LOGIN)
VALUES (17, 2, 'CS-431', 'Introduction to natural language processing',
        'http://edu.epfl.ch/coursebook/en/introduction-to-natural-language-processing-CS-431', 4.00, 'CS', 'PSF', null,
        'floflo');
INSERT INTO REX_ENSEIGNEMENTS (ID_DEPART, ID_ENSEIG, CODE, TITRE, LIEN, ECTS, CATEGORIE, PROFIL, CATEGORIE_TSH, LOGIN)
VALUES (17, 3, 'COM-480', 'Data visualization', 'http://edu.epfl.ch/coursebook/en/data-visualization-COM-480',
        4.00, 'TM', 'PSF', null, 'floflo');
INSERT INTO REX_ENSEIGNEMENTS (ID_DEPART, ID_ENSEIG, CODE, TITRE, LIEN, ECTS, CATEGORIE, PROFIL, CATEGORIE_TSH, LOGIN)
VALUES (17, 4, 'CS-401', 'Applied data analysis', 'http://edu.epfl.ch/coursebook/fr/applied-data-analysis-CS-401',
        6.00, 'TM', 'PSF', null, 'floflo');
INSERT INTO REX_ENSEIGNEMENTS (ID_DEPART, ID_ENSEIG, CODE, TITRE, LIEN, ECTS, CATEGORIE, PROFIL, CATEGORIE_TSH, LOGIN)
VALUES (2, 5, 'CS5242', 'Neural Networks and Deep Learning ', null, 6.00, 'CS', 'PSF', null, null);
INSERT INTO REX_ENSEIGNEMENTS (ID_DEPART, ID_ENSEIG, CODE, TITRE, LIEN, ECTS, CATEGORIE, PROFIL, CATEGORIE_TSH, LOGIN)
VALUES (2, 6, 'ST4231', 'Computational Intensive Statistical Methods ', null, 6.00, 'TM', 'PSF', null, null);
INSERT INTO REX_ENSEIGNEMENTS (ID_DEPART, ID_ENSEIG, CODE, TITRE, LIEN, ECTS, CATEGORIE, PROFIL, CATEGORIE_TSH, LOGIN)
VALUES (2, 7, 'MA4270', 'Data Modeling and Computation',
        'https://www.ece.nus.edu.sg/stfpage/vtan/data_modeling.pdf', 6.00, 'CS', 'PSF', null, null);
INSERT INTO REX_ENSEIGNEMENTS (ID_DEPART, ID_ENSEIG, CODE, TITRE, LIEN, ECTS, CATEGORIE, PROFIL, CATEGORIE_TSH, LOGIN)
VALUES (17, 8, 'EE-554', 'Automatic speech processing',
        'http://edu.epfl.ch/coursebook/en/automatic-speech-processing-EE-554', 3.00, 'TM', 'PCB', null, 'floflo');
INSERT INTO REX_ENSEIGNEMENTS (ID_DEPART, ID_ENSEIG, CODE, TITRE, LIEN, ECTS, CATEGORIE, PROFIL, CATEGORIE_TSH, LOGIN)
VALUES (17, 9, 'CS-210', 'Functional programming',
        'http://edu.epfl.ch/coursebook/en/functional-programming-CS-210', 5.00, 'TM', 'PCB', null, 'floflo');
INSERT INTO REX_ENSEIGNEMENTS (ID_DEPART, ID_ENSEIG, CODE, TITRE, LIEN, ECTS, CATEGORIE, PROFIL, CATEGORIE_TSH, LOGIN)
VALUES (2, 10, 'CS4234', 'Optimisation Algorithms', 'http://www.comp.nus.edu.sg/~stevenha/cs4234.html', 6.00,
        'CS', 'PCB', null, null);